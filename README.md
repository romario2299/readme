# Installation LendingPoint Web Component

The Web Component open the eCommerce GateWay Platform Modal

## Configuration

### 1. Import the Web Component
```
import { LendingPointGateWay } from "https://test-widget-2.netlify.app/lending-point-gateway.js";
```

### 2. Get an instance from LendingPointGateWay class.
```
const lendingPointGateWay = new LendingPointGateWay({
  loanAmount: 20000,
  onSuccess: () => {
    console.log("Running Function onSuccess");
  },
  onExit: () => {
    console.log("Running Function onFail");
  },
});
```
 The constructor of this class receive an object with 3 properties:
- ***loanAmount***: Number value with the loan amount.
- ***onSuccess***: Function Callback executed when virtual card process finish.
- ***onExit***: Function Callback executed when the user close the modal.  

### 3. You can show the modal, runnning
```
lendingPointGateWay.showModal();
```

## Example

This is an example where user open the LendingPoint Modal when click on a button. 

```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <button id="open-modal">Check Out</button>
    <script type="module">
      import { LendingPointGateWay } from "https://test-widget-2.netlify.app/lending-point-gateway.js";
      var button = document.getElementById("open-modal");
      button.addEventListener("click", function () {
        const lendingPointGateWay = new LendingPointGateWay({
          loanAmount: 20000,
          onSuccess: () => {
            console.log("Running Function onSuccess");
          },
          onExit: () => {
            console.log("Running Function onFail");
          },
        });
        lendingPointGateWay.showModal();
      });
    </script>
  </body>
</html>
```

